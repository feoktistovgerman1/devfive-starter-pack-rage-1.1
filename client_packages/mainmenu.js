let worldmenu;
var worldmenuWindow = null;
var worldmenuOppened = false;
mp.events.add('initworldmenu', () => {
    worldmenu = mp.browsers.new('package://browser/worldmenu.html');
});
// // //

mp.keys.bind(Keys.VK_F10, false, function () { // F10

    if (!global.loggedin) return;
    if(global.worldmenuOppened == true){
        mp.events.call('closeworldmenu');
    } else {
        mp.events.callRemote('worldmenuShow', '1');
    }
});

mp.events.add('worldmenuShow', (data,type) => {
    if (worldmenu == null) {
        worldmenu = mp.browsers.new('package://browser/worldmenu.html');
    }
  //  mp.gui.chat.push(data);
    worldmenu.execute(`worldmenu.menuid='${type}'`);
    worldmenu.execute(`worldmenu.stats=JSON.parse('${data}')`);

    worldmenu.execute("worldmenu.showworldmenu();");
    mp.gui.cursor.visible = true;
    worldmenuOppened = true;
});
mp.events.add('closeworldmenu', () => {
    worldmenu.execute('worldmenu.hide();');
    global.menuOpened = false;
    mp.gui.cursor.visible = false;
    worldmenuOppened = false;
});
mp.events.add('sendworldmenu', (action,id) => {
    mp.events.callRemote('actionworldmenu',action,id);
});


mp.events.add('worldmenuOpen', (data) => {
    if (worldmenu == null) {
        worldmenu = mp.browsers.new('package://browser/worldmenu.html');
    }
        global.worldmenuOppened = true;
    var json = JSON.parse(data);
    // // //
    var id = json[0];
    var canHome = json[3];
    var canBack = json[2];
    console.log(json[1]);

    var items = JSON.stringify(json[1]);
    console.log(items);
    
    // // //
    var exec = "openworldmenu('" + id + "'," + canHome + "," + canBack + ",'"  + items + "');";
    //mp.gui.chat.push(exec);
    worldmenu.execute(exec);
});
mp.events.add('worldmenuChange', (ind, data) => {
    var exec = "change(" + ind + ",'" + data + "');";
    //mp.gui.chat.push(exec);
    worldmenu.execute(exec);
});
mp.events.add('worldmenuClose', () => {
    if(worldmenu != null) worldmenu.execute('reset();');
});
// // //
mp.events.add('worldmenuCallback', (itemid, event, data) => {

    mp.events.callRemote('worldmenu', 'callback', itemid, event, data);
    //mp.gui.chat.push(itemid+":"+event+":"+data);
});
mp.events.add('worldmenuNavigation', (btn) => {
    mp.events.callRemote('worldmenu', 'navigation', btn);
});
// // //
/*mp.events.add("playerQuit", (player, exitType, reason) => {
    if (worldmenu !== null) {
        if (player.name === localplayer.name) {
            worldmenu.destroy();
            worldmenu = null;
            worldmenuOppened = false;
            worldmenuWindow = null;
        }
    }
});*/