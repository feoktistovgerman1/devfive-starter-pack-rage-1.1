﻿using GTANetworkAPI;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DevFive.Core;
using DevFive.SDK;
using DevFive.MoneySystem;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using DevFive.Houses;
using DevFive.Jobs;

namespace DevFive.GUI
{
    class worldmenu : Script
    {
        public static void Event_OnPlayerDisconnected(Player player, DisconnectionType type, string reason)
        {
            try
            {
                if (!isopen.ContainsKey(player)) return;
                isopen.Remove(player);
            }
            catch (Exception e) { Log.Write("PlayerDisconnected: " + e.Message, nLog.Type.Error); }
        }

        private static nLog Log = new nLog("worldmenu");
        public static Dictionary<Player, bool> isopen = new Dictionary<Player, bool>();

        [RemoteEvent("worldmenuShow")]
        public static void ClientEvent_worldmenuShow(Player player, string type)
        {
            try
            {

                var sumData = new List<string>();

                if (!Main.Players.ContainsKey(player)) return;
                Core.Character.Character acc = Main.Players[player];

                string status =
                    (acc.AdminLVL >= 1) ? "Администратор" :
                    (Main.Accounts[player].VipLvl > 0) ?
                    $"{Group.GroupNames[Main.Accounts[player].VipLvl]} до {Main.Accounts[player].VipDate.ToString("dd.MM.yyyy")}" :
                    $"{Group.GroupNames[Main.Accounts[player].VipLvl]}";
                long bank = (acc.Bank != 0) ? Bank.Accounts[acc.Bank].Balance : -1;
                string lic = "";
                for (int i = 0; i < acc.Licenses.Count; i++)
                    if (acc.Licenses[i]) lic += $"{Main.LicWords[i]} / ";
                if (lic == "") lic = "Отсутствуют";
                string work = (acc.WorkID > 0) ? Jobs.WorkManager.JobStats[acc.WorkID - 1] : "Безработный";
                string fraction = (acc.FractionID > 0) ? Fractions.Manager.FractionNames[acc.FractionID] : "Нет";
                string number = (acc.Sim == -1) ? "Нет сим-карты" : Main.Players[player].Sim.ToString();
                var getOnlineTime = $"{acc.LastHourMin}";
                string vip = Core.Group.GroupNames[Main.Accounts[player].VipLvl];
                if (Main.Accounts[player].VipLvl > 0 && Main.Accounts[player].VipDate > DateTime.Now) vip += "";
                else vip = "Игрок";
                int HoFCoins = Main.get_Coins(player);
                // DateTime vipdate = new DateTime((Main.Accounts[player].VipDate - DateTime.Now).Ticks);

                sumData = new List<string> {
                        $"{acc.FirstName}_{acc.LastName}", // 0
                        "", //1
                        $"{acc.LVL}", //2
                        work.ToString(), //3
                        fraction.ToString(), //4
                        acc.FractionLVL.ToString(), //5
                       $"{acc.Money}", //6
                       $"{bank}", //7
                        acc.UUID.ToString(), //8
                        acc.Health.ToString(), //9
                        acc.Armor.ToString(), //10
                        acc.Sim.ToString(), //11
                        acc.Water.ToString(), //12
                        acc.Eat.ToString(), //13
                        HoFCoins.ToString(), //14
                        (acc.Gender) ? "Мужской" : "Женский", //15
                        acc.CreateDate.ToString(), //16
                        acc.Bank.ToString(), //17
                        Main.Accounts[player].Login.ToString(), //18
                        "Зарегистрирован", //19
                        $"{acc.EXP}", //20
                        vip, //21
                        "0", //22
                        number.ToString(), //23
                        Main.Accounts[player].Email, //24
                        $"", //25
                        $"{Group.GroupNames[Main.Accounts[player].VipLvl]}", //26
                        $"", //27
                    };

                Trigger.ClientEvent(player, "worldmenuShow", JsonConvert.SerializeObject(sumData), type);
            }
            catch (Exception e) { Log.Write("actionworldmenu: " + e.Message, nLog.Type.Error); }
        }

        [RemoteEvent("actionworldmenuBuy")]
        public static void ClientEvent_actionworldmenuBuy(Player client, string typeAction, string data1, string data2)
        {
            int step = 0;
            Trigger.ClientEvent(client, "closeworldmenu");
            try
            {
                //  Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, $"Планшет "+ typeAction + " "+ id, 3000);

                var acc = Main.Accounts[client];
                switch (typeAction)
                {
                    case "rename":
                        {
                            string fullname = $"{data1}_{data2}";
                            if (acc.RedBucks < 1000)
                            {
                                Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                return;
                            }

                            if (!Main.PlayerNames.ContainsValue(client.Name)) return;
                            try
                            {

                                if (data1 == "null" || string.IsNullOrEmpty(data1))
                                {
                                    Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Вы не указали имя!", 3000);
                                    return;
                                }
                                else if (data2 == "null" || string.IsNullOrEmpty(data2))
                                {
                                    Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Вы не указали фамилию!", 3000);
                                    return;
                                }
                            }
                            catch (Exception e)
                            {
                                Log.Write("ERROR ON CHANGENAME DONATION\n" + e.ToString());
                                return;
                            }

                            if (Main.PlayerNames.ContainsValue(fullname))
                            {
                                Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Такое имя уже существует!", 3000);
                                return;
                            }

                            Player target = NAPI.Player.GetPlayerFromName(client.Name);

                            if (target == null || target.IsNull) return;
                            else
                            {
                                Core.Character.Character.toChange.Add(client.Name, fullname);
                                Main.Accounts[client].RedBucks -= 1000;
                                NAPI.Player.KickPlayer(target, "Смена ника");
                            }
                            GameLog.Money(acc.Login, "server", 1000, "donateName");
                            break;
                        }
                    case "changePass":
                        {
                            var oldPass = data1;
                            var newPass = data2;
                            var oldsha = DevFive.Core.nAccount.Account.GetSha256(oldPass);
                            var newsha = DevFive.Core.nAccount.Account.GetSha256(newPass);
                            if (Main.Accounts[client].Password == oldsha)
                            {
                                Main.Accounts[client].Password = newsha;
                                Main.Accounts[client].Save(client);
                                Notify.Send(client, NotifyType.Info, NotifyPosition.BottomCenter, $"Вы успешно сменили пароль", 3000);
                                Trigger.ClientEvent(client, "closeworldmenu");
                            }
                            else
                            {
                                Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, $"Старый пароль введён не верно!", 3000);
                            }
                            break;
                        }
                }
            }
            catch (Exception e) { Log.Write("actionworldmenuBuy: " + e.Message + " id: " + data1 + " step: " + step.ToString(), nLog.Type.Error); }
        }
        [RemoteEvent("actionworldmenu")]
        public static void ClientEvent_actionworldmenu(Player player, string typeAction, string id)
        {
            try
            {
                Trigger.ClientEvent(player, "closeworldmenu");
                //  Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, $"Планшет "+ typeAction + " "+ id, 3000);
                switch (typeAction)
                {
                    case "packages":
                        {
                            var acc = Main.Accounts[player];
                            Player client = player;
                            switch (id)
                            {
                                case "0":   // Старт
                                    {
                                        if (acc.RedBucks < 2290)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        ////////////////////////////////////////////// Silver VIP 60 дней
                                        if (acc.VipLvl < 2)
                                        {
                                            Main.Accounts[client].VipLvl = 2;
                                            Main.Accounts[client].VipDate = DateTime.Now.AddDays(60);
                                            Dashboard.sendStats(client);
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам был выдан SILVER VIP на 60 дней", 3000);
                                        }
                                        //////////////////////////////////////////////
                                        Main.Accounts[client].RedBucks -= 2290;
                                        Main.Accounts[player].Save(player);
                                        GameLog.Money(acc.Login, "server", 2290, "donateSVip");
                                        //////////////////////////////////////////////
                                        Main.Players[player].Licenses[7] = true;
                                        Main.Players[player].Licenses[1] = true;
                                        Main.Players[player].Save(player);
                                        MoneySystem.Wallet.Change(player, +500000);
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам было зачислено $500 000", 3000);
                                        GameLog.Money($"donate", $"player({Main.Players[player].UUID})", 500000, $"donate");
                                        break;
                                    }
                                case "1": // На коне
                                    {
                                        if (acc.RedBucks < 4990)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        ////////////////////////////////////////////// Gold VIP 30 дней
                                        if (Main.Accounts[client].VipLvl < 3)
                                        {
                                            Main.Accounts[client].VipLvl = 3;
                                            Main.Accounts[client].VipDate = DateTime.Now.AddDays(30);
                                            Dashboard.sendStats(client);
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам был выдан GOLD VIP на 30 дней", 3000);
                                        }
                                        //////////////////////////////////////////////
                                        Main.Accounts[client].RedBucks -= 4990;
                                        Main.Accounts[player].Save(player);
                                        GameLog.Money(acc.Login, "server", 4990, "donateSVip");
                                        //////////////////////////////////////////////
                                        Main.Players[player].Licenses[7] = true;
                                        Main.Players[player].Licenses[1] = true;
                                        Main.Players[player].Save(player);
                                        MoneySystem.Wallet.Change(player, +700000);
                                        GameLog.Money($"donate", $"player({Main.Players[player].UUID})", 700000, $"donate");
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам было зачислено $700 000", 3000);
                                        //////////////////////////////////////////////camry18
                                        var vNumber = VehicleManager.Create(player.Name, "camry18", new Color(0, 0, 0), new Color(0, 0, 0));
                                        nInventory.Add(player, new nItem(ItemType.CarKey, 1, $"{vNumber}_{VehicleManager.Vehicles[vNumber].KeyNum}"));
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вы выиграли автомобиль Toyota Camry 2018 с гос. номером {vNumber}", 3000);
                                        break;
                                    }
                                case "2":  // Совершенство
                                    {
                                        if (acc.RedBucks < 9990)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }

                                        ////////////////////////////////////////////// Platinum VIP 60 дней
                                        if (Main.Accounts[client].VipLvl < 3)
                                        {
                                            Main.Accounts[client].VipLvl = 4;
                                            Main.Accounts[client].VipDate = DateTime.Now.AddDays(60);
                                            Dashboard.sendStats(client);
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам был выдан Platinum VIP на 60 дней", 3000);
                                        }
                                        //////////////////////////////////////////////
                                        Main.Accounts[client].RedBucks -= 9990;
                                        Main.Accounts[player].Save(player);
                                        GameLog.Money(acc.Login, "server", 9990, "donateSVip");
                                        //////////////////////////////////////////////
                                        Main.Players[player].Licenses[7] = true;
                                        Main.Players[player].Licenses[1] = true;
                                        Main.Players[player].Save(player);
                                        MoneySystem.Wallet.Change(player, +1000000);
                                        GameLog.Money($"donate", $"player({Main.Players[player].UUID})", 1000000, $"donate");
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам было зачислено $1 000 000", 3000);
                                        //////////////////////////////////////////////camry18
                                        var vNumber = VehicleManager.Create(player.Name, "camry18", new Color(0, 0, 0), new Color(0, 0, 0));
                                        nInventory.Add(player, new nItem(ItemType.CarKey, 1, $"{vNumber}_{VehicleManager.Vehicles[vNumber].KeyNum}"));
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вы выиграли автомобиль Toyota Camry XSE с гос. номером {vNumber}", 3000);
                                        //////////////////////////////////////////////Дом
                                        ///
                                        var playerHouse = HouseManager.GetHouse(player);
                                        if (playerHouse != null)
                                        {
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"К сожалению, мы не можем вам подарить второй дом", 3000);
                                        }
                                        else
                                        {
                                            foreach (House h in HouseManager.Houses)
                                            {
                                                if (string.IsNullOrEmpty(h.Owner) && h.Type > 3)
                                                {
                                                    var vehicles = VehicleManager.getAllPlayerVehicles(player.Name).Count;
                                                    var maxcars = GarageManager.GarageTypes[GarageManager.Garages[h.GarageID].Type].MaxCars;
                                                    if (vehicles > maxcars)
                                                    {
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        if (HouseManager.HouseTypeList[h.Type].PetPosition != null) h.PetName = Main.Players[player].PetName;
                                                        Notify.Send(player, NotifyType.Success, NotifyPosition.BottomCenter, $"Вы получили в собственность Дом №{h.ID}, не забудьте оплатить налог " +
                                                            $"за недвижимость в ближайшем банкомате", 3000);
                                                        HouseManager.CheckAndKick(player);
                                                        h.SetLock(true);
                                                        h.SetOwner(player);
                                                        h.SendPlayer(player);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case "3":  // PLATINUM
                                    {
                                        if (acc.RedBucks < 35000)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }

                                        ////////////////////////////////////////////// Platinum VIP 180 дней
                                        if (Main.Accounts[client].VipLvl < 3)
                                        {
                                            Main.Accounts[client].VipLvl = 4;
                                            if (DateTime.Now < Main.Accounts[client].VipDate)
                                                Main.Accounts[client].VipDate = DateTime.Now.AddDays(180);
                                            else
                                                Main.Accounts[client].VipDate = Main.Accounts[client].VipDate.AddDays(180);
                                            Dashboard.sendStats(client);
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам был выдан Platinum VIP на 60 дней", 3000);
                                        }
                                        //////////////////////////////////////////////
                                        Main.Accounts[client].RedBucks -= 35000;
                                        Main.Accounts[player].Save(player);
                                        GameLog.Money(acc.Login, "server", 35000, "donateSVip");
                                        //////////////////////////////////////////////
                                        Main.Players[player].Licenses[6] = true;
                                        Main.Players[player].Licenses[5] = true;
                                        Main.Players[player].Licenses[7] = true;
                                        Main.Players[player].Licenses[1] = true;
                                        Main.Players[player].Save(player);
                                        MoneySystem.Wallet.Change(player, +3000000);
                                        GameLog.Money($"donate", $"player({Main.Players[player].UUID})", 3000000, $"donate");
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вам было зачислено $3 000 000", 3000);
                                        //////////////////////////////////////////////camry18
                                        var vNumber = VehicleManager.Create(player.Name, "g65amg", new Color(0, 0, 0), new Color(0, 0, 0));
                                        nInventory.Add(player, new nItem(ItemType.CarKey, 1, $"{vNumber}_{VehicleManager.Vehicles[vNumber].KeyNum}"));
                                        Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"Вы выиграли автомобиль Mercedes G65 AMG с гос. номером {vNumber}", 3000);
                                        //////////////////////////////////////////////
                                        ///
                                        var playerHouse = HouseManager.GetHouse(player);
                                        if (playerHouse != null)
                                        {
                                            Notify.Send(player, NotifyType.Info, NotifyPosition.BottomCenter, $"К сожалению, мы не можем вам подарить второй дом", 3000);
                                        }
                                        else
                                        {
                                            foreach (House h in HouseManager.Houses)
                                            {
                                                if (string.IsNullOrEmpty(h.Owner) && h.Type >= 5)
                                                {
                                                    var vehicles = VehicleManager.getAllPlayerVehicles(player.Name).Count;
                                                    var maxcars = GarageManager.GarageTypes[GarageManager.Garages[h.GarageID].Type].MaxCars;
                                                    if (vehicles > maxcars)
                                                    {
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        if (HouseManager.HouseTypeList[h.Type].PetPosition != null) h.PetName = Main.Players[player].PetName;
                                                        Notify.Send(player, NotifyType.Success, NotifyPosition.BottomCenter, $"Вы получили в собственность Дом №{h.ID}, не забудьте оплатить налог " +
                                                            $"за недвижимость в ближайшем банкомате", 3000);
                                                        HouseManager.CheckAndKick(player);
                                                        h.SetLock(true);
                                                        h.SetOwner(player);
                                                        h.SendPlayer(player);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    case "exchange":
                        {
                            var acc = Main.Accounts[player];
                            int amount = 0;
                            if (!Int32.TryParse(id, out amount))
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, "Возникла ошибка, попоробуйте еще раз", 3000);
                                return;
                            }
                            amount = Math.Abs(amount);
                            if (amount < 0)
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, "Введите количество, равное 1 или больше.", 3000);
                                return;
                            }
                            if (Main.Accounts[player].RedBucks < amount)
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                return;
                            }
                            Main.Accounts[player].RedBucks = Convert.ToInt64(Convert.ToInt32(Main.get_Coins(player)) - amount);

                            Main.Accounts[player].Save(player);
                            Main.Players[player].Save(player);
                            GameLog.Money(Main.Accounts[player].Login, "server", amount, "donateConvert");
                            amount = amount * 100;
                            MoneySystem.Wallet.Change(player, +amount);
                            GameLog.Money($"donate", $"player({Main.Players[player].UUID})", amount, $"donate");
                            Notify.Send(player, NotifyType.Success, NotifyPosition.BottomCenter, $"Обмен выполнен успешно. Вам на счёт было зачислено ${amount}", 3000);
                            break;
                        }
                    case "restyle":
                        {
                            var acc = Main.Accounts[player];
                            if (acc.RedBucks < 500)
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                return;
                            }
                            Main.Accounts[player].RedBucks -= 500;
                            GameLog.Money(acc.Login, "server", 500, "donateChar");
                            Customization.SendToCreator(player);
                            Main.Accounts[player].Save(player);
                            Main.Players[player].Save(player);
                            Notify.Send(player, NotifyType.Success, NotifyPosition.BottomCenter, "Удачного вам преображения.", 3000);
                            break;
                        }
                    case "unwarn":
                        {
                            var acc = Main.Accounts[player];
                            Player client = player;
                            if (Main.Players[client].Warns <= 0)
                            {
                                Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "У вас нет Warn'a!", 3000);
                                return;
                            }
                            if (acc.RedBucks < 300)
                            {
                                Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                return;
                            }
                            Main.Accounts[client].RedBucks -= 300;
                            GameLog.Money(acc.Login, "server", 300, "donateWarn");
                            Main.Players[client].Warns -= 1;
                            Main.Accounts[client].Save(client);
                            Main.Players[client].Save(client);
                            Dashboard.sendStats(client);
                            Notify.Send(client, NotifyType.Success, NotifyPosition.BottomCenter, "С вас был снят WARN.", 3000);
                            break;
                        }
                    case "buyVip":
                        {
                            var acc = Main.Accounts[player];
                            Player client = player;
                            switch (id)
                            {
                                case "0": // BRONZE
                                    {
                                        if (Main.Accounts[client].VipLvl >= 1)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "У вас уже куплен VIP статус!", 3000);
                                            return;
                                        }
                                        if (acc.RedBucks < 400)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        Main.Accounts[client].RedBucks -= 400;
                                        GameLog.Money(acc.Login, "server", 400, "donateBVip");
                                        Main.Accounts[client].VipLvl = 1;
                                        Main.Accounts[client].VipDate = DateTime.Now.AddDays(30);
                                        Main.Accounts[client].Save(client);
                                        Main.Players[client].Save(client);
                                        Dashboard.sendStats(client);
                                        Notify.Send(client, NotifyType.Success, NotifyPosition.BottomCenter, "Статус куплен успешно", 3000);
                                        break;
                                    }
                                case "1": // SILVER
                                    {
                                        if (acc.VipLvl >= 1)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "У вас уже куплен VIP статус!", 3000);
                                            return;
                                        }
                                        if (acc.RedBucks < 700)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        Main.Accounts[client].RedBucks -= 700;
                                        GameLog.Money(acc.Login, "server", 700, "donateSVip");
                                        Main.Accounts[client].VipLvl = 2;
                                        Main.Accounts[client].VipDate = DateTime.Now.AddDays(30);
                                        Dashboard.sendStats(client);
                                        Notify.Send(client, NotifyType.Success, NotifyPosition.BottomCenter, "Статус куплен успешно", 3000);
                                        break;
                                    }
                                case "2": // GOLD
                                    {
                                        if (Main.Accounts[client].VipLvl >= 1)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "У вас уже куплен VIP статус!", 3000);
                                            return;
                                        }
                                        if (acc.RedBucks < 1200)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        Main.Accounts[client].RedBucks -= 1200;
                                        GameLog.Money(acc.Login, "server", 1200, "donateGVip");
                                        Main.Accounts[client].VipLvl = 3;
                                        Main.Accounts[client].VipDate = DateTime.Now.AddDays(30);
                                        Dashboard.sendStats(client);
                                        Notify.Send(client, NotifyType.Success, NotifyPosition.BottomCenter, "Статус куплен успешно", 3000);
                                        break;
                                    }
                                case "3": // PLATINUM
                                    {
                                        if (Main.Accounts[client].VipLvl >= 1)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "У вас уже куплен VIP статус!", 3000);
                                            return;
                                        }
                                        if (acc.RedBucks < 1500)
                                        {
                                            Notify.Send(client, NotifyType.Error, NotifyPosition.BottomCenter, "Недостаточно средств на счету!", 3000);
                                            return;
                                        }
                                        Main.Accounts[client].RedBucks -= 1500;
                                        GameLog.Money(acc.Login, "server", 1500, "donatePVip");
                                        Main.Accounts[client].VipLvl = 4;
                                        Main.Accounts[client].VipDate = DateTime.Now.AddDays(30);
                                        Dashboard.sendStats(client);
                                        Notify.Send(client, NotifyType.Success, NotifyPosition.BottomCenter, "Статус куплен успешно", 3000);
                                        break;
                                    }
                            }
                            return;
                        }
                    case "passport":
                        {
                            var acc = Main.Players[player];
                            if (player.IsInVehicle) return;
                            {
                                string gender = (acc.Gender) ? "Мужской" : "Женский";
                                string fraction = (acc.FractionID > 0) ? Fractions.Manager.FractionNames[acc.FractionID] : "Нет";
                                string work = (acc.WorkID > 0) ? Jobs.WorkManager.JobStats[acc.WorkID] : "Безработный";
                                List<object> data = new List<object>
                            {
                                acc.UUID,
                                acc.FirstName,
                                acc.LastName,
                                acc.CreateDate.ToString("dd.MM.yyyy"),
                                gender,
                                fraction,
                                work
                            };
                                string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                                Trigger.ClientEvent(player, "passport", json);
                                Trigger.ClientEvent(player, "newPassport", player, acc.UUID);
                            }
                            return;
                        }
                    case "licenses":
                        if (player.IsInVehicle) return;
                        {

                            var acc = Main.Players[player];
                            string gender = (acc.Gender) ? "Мужской" : "Женский";

                            var lic = "";
                            for (int i = 0; i < acc.Licenses.Count; i++)
                                if (acc.Licenses[i]) lic += $"{Main.LicWords[i]} / ";
                            if (lic == "") lic = "Отсутствуют";

                            List<string> data = new List<string>
                            {
                                acc.FirstName,
                                acc.LastName,
                                acc.CreateDate.ToString("dd.MM.yyyy"),
                                gender,
                                lic
                            };

                            string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                            Trigger.ClientEvent(player, "licenses", json);
                        }
                        return;
                    case "reportWorld":
                        if (id.Length > 150)
                        {
                            Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, $"Слишком длинное сообщение", 3000);
                            return;
                        }
                        if (Main.Accounts[player].VipLvl == 0 && player.HasData("NEXT_REPORT"))
                        {
                            DateTime nextReport = player.GetData<DateTime>("NEXT_REPORT");
                            if (DateTime.Now < nextReport)
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, $"Попробуйте отправить жалобу через некоторое время", 3000);
                                return;
                            }
                        }
                        if (player.HasData("MUTE_TIMER"))
                        {
                            Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, "Нельзя отправлять репорт во время Mute, дождитесь его окончания", 3000);
                            return;
                        }
                        ReportSys.AddReport(player, id);
                        break;
                    case "promoUse":
                        try
                        {
                            var promo = id.ToLower();
                            var Acc = Main.Accounts[player];
                            if (Main.PromoCodes.ContainsKey(promo))
                            {
                                if (!Acc.PromoCodes.Contains(promo))
                                {
                                    if (promo.ToLower() == "HouseOfFreedom")
                                    {

                                        Notify.Send(player, NotifyType.Success, NotifyPosition.BottomCenter, $"Поздравляем, вы активировали промокод HouseOfFreedom и получили: [+] 2 уровень персонажа; " +
                                            $"[+] Паспорт гражданина;" +
                                            $"[+] VIP статус (3 дня);" +
                                            $"Администрация HouseOfFreedom ROLEPLAY поздравляет вас с наступающим Новым Годом и желает вам приятной игры на нашем уютном проекте..", 10000);
                                        Acc.PromoCodes.Add(promo);
                                        Acc.VipLvl = 2;
                                        Acc.VipDate = DateTime.Now.AddDays(3);

                                        if (Main.Players[player].LVL < 2)
                                        {
                                            Main.Players[player].LVL = 2;
                                            Main.Players[player].EXP = 5;
                                        }
                                        Acc.Save(player);

                                        return;
                                    }
                                    else
                                    {
                                        if (Acc.PromoCodes[0].Equals("noref"))
                                        {
                                            Acc.PromoCodes[0] = promo;
                                        }
                                        else
                                        {
                                            if (Main.PromoCodes[promo].Item1 > 1)
                                            {
                                                Acc.PromoCodes.Add(promo);
                                            }
                                            else
                                            {
                                                Notify.Send(player, NotifyType.Error, NotifyPosition.TopCenter, $"Вы уже активировали другой промокод", 3000);
                                                return;
                                            }
                                        }
                                        Acc.VipLvl = 1;
                                        Acc.VipDate = DateTime.Now.AddDays(5);
                                        Acc.Save(player);
                                        Notify.Send(player, NotifyType.Success, NotifyPosition.TopCenter, $"Вы успешно активировали промомод", 3000);
                                    }
                                    return;

                                }
                                else
                                {
                                    Notify.Send(player, NotifyType.Error, NotifyPosition.TopCenter, $"Промокод уже был активирован ранее", 3000);
                                    return;
                                }
                            }
                            else
                            {
                                Notify.Send(player, NotifyType.Error, NotifyPosition.BottomCenter, $"Промокод не обнаружен", 3000);
                                return;
                            }

                        }
                        catch (Exception e) { Log.Write("EXCEPTION AT \"CMD\":\n" + e.ToString(), nLog.Type.Error); }
                        break;
                }
            }
            catch (Exception e) { Log.Write("actionworldmenuAction " + typeAction + ": " + e.Message, nLog.Type.Error); }
        }

        public static void sendStats(Player player)
        {
            try
            {
                if (!Main.Players.ContainsKey(player)) return;
                Core.Character.Character acc = Main.Players[player];

                string status =
                    (acc.AdminLVL >= 1) ? "Администратор" :
                    (Main.Accounts[player].VipLvl > 0) ? $"{Group.GroupNames[Main.Accounts[player].VipLvl]} до {Main.Accounts[player].VipDate.ToString("dd.MM.yyyy")}" :
                    $"{Group.GroupNames[Main.Accounts[player].VipLvl]}";

                long bank = (acc.Bank != 0) ? Bank.Accounts[acc.Bank].Balance : -1;

                string lic = "";
                for (int i = 0; i < acc.Licenses.Count; i++)
                    if (acc.Licenses[i]) lic += $"{Main.LicWords[i]} / ";
                if (lic == "") lic = "Отсутствуют";

                string work = (acc.WorkID > 0) ? Jobs.WorkManager.JobStats[acc.WorkID - 1] : "Безработный";
                string fraction = (acc.FractionID > 0) ? Fractions.Manager.FractionNames[acc.FractionID] : "Нет";

                string number = (acc.Sim == -1) ? "Нет сим-карты" : Main.Players[player].Sim.ToString();

                string vip = Core.Group.GroupNames[Main.Accounts[player].VipLvl];
                if (Main.Accounts[player].VipLvl > 0 && Main.Accounts[player].VipDate > DateTime.Now) vip += $" (до {Main.Accounts[player].VipDate})";
                else vip = "Игрок";
                //  DateTime vipdate = new DateTime((Main.Accounts[player].VipDate - DateTime.Now).Ticks);

                List<object> data = new List<object>
                {
                        $"{acc.FirstName}_{acc.LastName}", // 0
                        "", //1
                        $"{acc.LVL}", //2
                        work.ToString(), //3
                        fraction.ToString(), //4
                        acc.FractionLVL.ToString(), //5
                       $"{acc.Money}", //6
                       $"{bank}", //7
                        acc.UUID.ToString(), //8
                        acc.Health.ToString(), //9
                        acc.Armor.ToString(), //10
                        acc.Sim.ToString(), //11
                        acc.Water.ToString(), //12
                        acc.Eat.ToString(), //13
                        Main.Accounts[player].RedBucks.ToString(), //14
                        (acc.Gender) ? "Мужской" : "Женский", //15
                        acc.CreateDate.ToString(), //16
                        acc.Bank.ToString(), //17
                        Main.Accounts[player].Login.ToString(), //18
                        "Зарегистрирован", //19
                        $"{acc.EXP}", //20
                        vip, //21
                        "0", //22
                        number.ToString(), //23
                        Main.Accounts[player].Email, //24
                        $"", //25
                        $"{Group.GroupNames[Main.Accounts[player].VipLvl]}", //26
                        $"", //27
            };

                string json = JsonConvert.SerializeObject(data);
                Log.Debug("data is: " + json.ToString());
                Trigger.ClientEvent(player, "board", 2, json);

                data.Clear();

            }
            catch (Exception e)
            {
                Log.Write("EXCEPTION AT \"DASHBOARD_SENDSTATS\":\n" + e.ToString(), nLog.Type.Error);
            }
        }
        public static async Task SendStatsAsync(Player player)
        {
            try
            {
                if (!Main.Players.ContainsKey(player)) return;
                Core.Character.Character acc = Main.Players[player];

                string status =
                    (acc.AdminLVL >= 1) ? "Администратор" :
                    (Main.Accounts[player].VipLvl > 0) ? $"{Group.GroupNames[Main.Accounts[player].VipLvl]} до {Main.Accounts[player].VipDate.ToString("dd.MM.yyyy")}" :
                    $"{Group.GroupNames[Main.Accounts[player].VipLvl]}";

                long bank = (acc.Bank != 0) ? Bank.Accounts[acc.Bank].Balance : 0;

                string lic = "";
                for (int i = 0; i < acc.Licenses.Count; i++)
                    if (acc.Licenses[i]) lic += $"{Main.LicWords[i]} / ";
                if (lic == "") lic = "Отсутствуют";

                string work = (acc.WorkID > 0) ? Jobs.WorkManager.JobStats[acc.WorkID - 1] : "Безработный";
                string fraction = (acc.FractionID > 0) ? Fractions.Manager.FractionNames[acc.FractionID] : "Нет";

                string number = (acc.Sim == -1) ? "Нет сим-карты" : Main.Players[player].Sim.ToString();

                string vip = Core.Group.GroupNames[Main.Accounts[player].VipLvl];
                if (Main.Accounts[player].VipLvl > 0 && Main.Accounts[player].VipDate > DateTime.Now) vip += $" (до {Main.Accounts[player].VipDate})";
                else vip = "Игрок";
                //    DateTime vipdate = new DateTime((Main.Accounts[player].VipDate - DateTime.Now).Ticks);
                List<object> data = new List<object>
                {
                        $"{acc.FirstName}_{acc.LastName}", // 0
                        "", //1
                        $"{acc.LVL}", //2
                        work.ToString(), //3
                        fraction.ToString(), //4
                        acc.FractionLVL.ToString(), //5
                       $"{acc.Money}", //6
                       $"{bank}", //7
                        acc.UUID.ToString(), //8
                        acc.Health.ToString(), //9
                        acc.Armor.ToString(), //10
                        acc.Sim.ToString(), //11
                        acc.Water.ToString(), //12
                        acc.Eat.ToString(), //13
                        Main.Accounts[player].RedBucks.ToString(), //14
                        (acc.Gender) ? "Мужской" : "Женский", //15
                        acc.CreateDate.ToString(), //16
                        acc.Bank.ToString(), //17
                        Main.Accounts[player].Login.ToString(), //18
                        "Зарегистрирован", //19
                        $"{acc.EXP}", //20
                        vip, //21
                        "0", //22
                        number.ToString(), //23
                        Main.Accounts[player].Email, //24
                        $"", //25
                        $"{Group.GroupNames[Main.Accounts[player].VipLvl]}", //26
                        $"", //27
                };

                string json = JsonConvert.SerializeObject(data);
                Log.Debug("data is: " + json.ToString());
                Trigger.ClientEvent(player, "board", 2, json);

                data.Clear();
            }
            catch (Exception e)
            {
                Log.Write("EXCEPTION AT \"DASHBOARD_SENDSTATS\":\n" + e.ToString(), nLog.Type.Error);
            }
        }
    }
}
