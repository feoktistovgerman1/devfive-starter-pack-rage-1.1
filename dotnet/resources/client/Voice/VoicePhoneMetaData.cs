﻿using GTANetworkAPI;

namespace DevFive.Voice
{
    struct VoicePhoneMetaData
    {
        public Player Target;
        public string CallingState;
    }
}
